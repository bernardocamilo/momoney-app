# Momoney: Android Module #

An android development project done by UFRJ students from 
Computer and Information Engineering.

Developers:

* Bernardo Camilo
* Rachel Castro

Supervisor:

* Sérgio Barbosa Villas-Boas

Dependencies:

* ActionBarSherlock (http://actionbarsherlock.com/)
* HoloGraph Library (https://bitbucket.org/danielnadeau/holographlibrary/wiki/Home)

Instructions:

* Download dependencies
* Import dependencies to Eclipse
* Clone this project
* Import the project to Eclipse
* Add dependencies to the project 
(Right-click the project > Go to "Properties" > Go to "Android" section > Add dependencies in the "Library" sub-section)