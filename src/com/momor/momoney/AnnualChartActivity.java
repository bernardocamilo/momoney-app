package com.momor.momoney;

import java.util.ArrayList;
import java.util.Collections;

import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class AnnualChartActivity extends Activity {

	ArrayList<Expense> expensesArray;
	int categoryId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_annual_chart_activity);
		Intent intent_data = getIntent();
	    Bundle bundle = intent_data.getBundleExtra("bundleData");
	    categoryId = bundle.getInt("Category_Id");
	    expensesArray = bundle.getParcelableArrayList("Expenses");
	    getValuesFromArrayYear(expensesArray);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.annual_chart_activity, menu);
		return true;
	}
	
	private void getValuesFromArrayYear (ArrayList<Expense> ExpensesArray){
		Double totalValue = 0.0;
        ArrayList<Double> totalExpenses = new ArrayList<Double>(Collections.nCopies(12, 0.0));
	    int size = ExpensesArray.size();
	    for (int i = 0; i<size; i++){
	    	Expense expense = ExpensesArray.get(i);	
	    	if (expense.getCategory_Id() == categoryId){
	    		totalValue = totalExpenses.get(expense.getMonth());
	    		totalValue = totalValue + expense.getValue();
	    		totalExpenses.set(expense.getMonth(), totalValue);
	    	}
	    }
	    clearChart();
	    plotChartYear(totalExpenses);
	}
	
	private void plotChartYear(ArrayList<Double> TotalExpenses) {
		ArrayList<Bar> points = new ArrayList<Bar>();
		Bar d;
		for (int i = 0; i < TotalExpenses.size(); i++) {
			d = new Bar();
			d.setValue((TotalExpenses.get(i)).floatValue());
			d.setName(String.valueOf(i+1));
			points.add(d);
		}
		BarGraph g = (BarGraph)findViewById(R.id.annual_chart);
		g.setBars(points);
		
	}
	
	private void clearChart() {
		ArrayList<Bar> points = new ArrayList<Bar>();
		BarGraph g = (BarGraph) findViewById(R.id.annual_chart);
		g.setBars(points);
	}
}
