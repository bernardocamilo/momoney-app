package com.momor.momoney;

import android.os.Parcel;
import android.os.Parcelable;

public class Category implements Parcelable {
	
	private int id;
	private String name;
	
	public int describeContents(){
		return 0;
	}
	
	public void writeToParcel(Parcel out, int flags){
		out.writeInt(id);
		out.writeString(name);
	}
	
	public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
		public Category createFromParcel(Parcel in) {
			return new Category(in);
		}
		public Category[] newArray(int size){
			return new Category[size];
		}
	};
	
	public Category(Parcel in){
		id = in.readInt();
		name = in.readString();
	}

	public Category(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
