package com.momor.momoney;

import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;

import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ChartActivity extends SherlockActivity {
	
	String categoryName;
	int categoryId;
	int month;
	String monthName;
	ArrayList<Expense> expensesArray;
	String[] rawMonths = new DateFormatSymbols().getMonths();
	String[] months = Arrays.copyOfRange(rawMonths, 0, rawMonths.length -1);
	private AlertDialog alert;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chart);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getSupportActionBar().getThemedContext(), R.layout.sherlock_spinner_item, months);
        getSupportActionBar().setNavigationMode(com.actionbarsherlock.app.ActionBar.NAVIGATION_MODE_LIST);
        ActionBar.OnNavigationListener navigationListener = new ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
    			getValuesFromArray (expensesArray, itemPosition);
    			return true;
            }
        };
        getSupportActionBar().setListNavigationCallbacks(adapter, navigationListener);
        adapter.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);		
		
	    Intent intent_data = getIntent();
	    Bundle bundle = intent_data.getBundleExtra("bundleCategExp");
	    categoryId = bundle.getInt("Category_Id");
	    categoryName = bundle.getString("Category_Name");
	    expensesArray = bundle.getParcelableArrayList("Expenses");
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home:
	    	Intent intent_credits = new Intent(ChartActivity.this, HomeActivity.class);
			startActivityForResult(intent_credits, 0);
	        return true;
	    case R.id.menu_annual:
	    	Bundle bundle = new Bundle();
    		bundle.putParcelableArrayList("Expenses", expensesArray);
    		bundle.putInt("Category_Id", categoryId);
    		Intent intent_expense = new Intent(ChartActivity.this, AnnualChartActivity.class);
			intent_expense.putExtra("bundleData", bundle);
			startActivityForResult(intent_expense, 0);
	        return true;
	    default: return super.onOptionsItemSelected(item);  
	    }
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    
	    Calendar c = Calendar.getInstance();
		month = c.get(Calendar.MONTH);
		getSupportActionBar().setSelectedNavigationItem(month);
	    
	}
	
    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        getSupportMenuInflater().inflate(R.menu.chart, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    private void monthAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.title_month_alert);
		builder.setMessage(R.string.message_month_alert);
		builder.setPositiveButton(R.string.button_month_alert, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				getSupportActionBar().setSelectedNavigationItem(month);
				alert.dismiss();
			}
		});
		alert = builder.create();
		alert.show();
	}
	
	private void plotChart(ArrayList<Expense> ExpensesArray) {
		ArrayList<Bar> points = new ArrayList<Bar>();
		Bar d;
		for (int i = 0; i < ExpensesArray.size(); i++) {
			d = new Bar();
			d.setValue((float) ExpensesArray.get(i).getValue());
			d.setName(String.valueOf(ExpensesArray.get(i).getDay()));
			points.add(d);
		}
		
		BarGraph g = (BarGraph)findViewById(R.id.bar_chart);
		g.setBars(points);
		
	}
	
	private void clearChart() {
		ArrayList<Bar> points = new ArrayList<Bar>();
		BarGraph g = (BarGraph) findViewById(R.id.bar_chart);
		g.setBars(points);
	}
	
	private void getValues(ArrayList<Expense> ExpensesArray){
		int length = ExpensesArray.size();
		ArrayList<String> expensesValue = new ArrayList<String>(length);
		for (int i = 0; i < length; i++) {
			String list_line = String.valueOf(getResources().getString(R.string.expense_list_msg1) 
					+ String.valueOf(ExpensesArray.get(i).getValue())) + getResources().getString(R.string.expense_list_msg2) 
					+ String.valueOf(ExpensesArray.get(i).getDay() + " - " + ExpensesArray.get(i).getDescription());
			expensesValue.add(list_line);
		}
		ListView valuesListView = (ListView) findViewById (R.id.values_list_view);
        if (valuesListView != null) {
            valuesListView.setAdapter(new ArrayAdapter<String>(ChartActivity.this,
              android.R.layout.simple_list_item_1, expensesValue));
        }
	}
	
	private void getValuesFromArray (ArrayList<Expense> ExpensesArray, int month_selected){
		Double totalValue = 0.0;
        Double totalCategory = 0.0;
        final ArrayList<Expense> expenseSelected = new ArrayList<Expense>();
	    int size = ExpensesArray.size();
	    for (int i = 0; i<size; i++){
	    	Expense expense = ExpensesArray.get(i);
	    	if (expense.getMonth() == month_selected){
	    		totalValue += expense.getValue();
	    		if (expense.getCategory_Id() == categoryId){
	    			totalCategory += expense.getValue();
	    			expenseSelected.add(expense);
	    		}
	    	}
	    }
	    if (expenseSelected.size() != 0) {
	    	clearChart();
	    	DecimalFormat df = new DecimalFormat("#.##");
		    String percentage = df.format((totalCategory/totalValue)*100);
		    TextView p = (TextView)findViewById(R.id.textViewpercentage);
		    p.setText(categoryName + getResources().getString(R.string.expense_info_msg1) 
		    		+ percentage + getResources().getString(R.string.expense_info_msg2));
    	    getValues(expenseSelected);
    	    plotChart(expenseSelected);	
	    }
	    else{
	    	monthAlert();
	    }    	
	}
}
