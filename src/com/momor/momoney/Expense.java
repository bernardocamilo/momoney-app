package com.momor.momoney;

import android.os.Parcel;
import android.os.Parcelable;

public class Expense implements Parcelable{
	
	private int id;
	private int category_id;
	private int day;
	private int month;
	private int year;
	private String description;
	private int payment_type_id;
	private double value;
	
	public int describeContents(){
		return 0;
	}
	
	public void writeToParcel(Parcel out, int flags){
		out.writeInt(id);
		out.writeInt(category_id);
		out.writeInt(day);
		out.writeInt(month);
		out.writeInt(year);
		out.writeString(description);
		out.writeInt(payment_type_id);
		out.writeDouble(value);
	}
	public static final Parcelable.Creator<Expense> CREATOR = new Parcelable.Creator<Expense>() {
		public Expense createFromParcel(Parcel in) {
			return new Expense(in);
		}
		public Expense[] newArray(int size){
			return new Expense[size];
		}
	};
	public Expense(Parcel in){
		id = in.readInt();
		category_id = in.readInt();
		day = in.readInt();
		month = in.readInt();
		year = in.readInt();
		description = in.readString();
		payment_type_id = in.readInt();
		value = in.readDouble();
	}
	
	public Expense(int id, int category_id, int day, int month, int year, String description, int payment_type_id, double value){
		this.id = id;
		this.category_id = category_id;
		this.day = day;
		this.month = month;
		this.year = year;
		this.description = description;
		this.payment_type_id = payment_type_id;
		this.value = value;
	}
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}
	public int getCategory_Id(){
		return category_id;
	}
	public void setCategory_Id(int category_id){
		this.category_id = category_id;
	}
	public int getDay(){
		return day;
	}
	public void setDay(int day){
		this.day = day;
	}
	public int getMonth(){
		return month;
	}
	public void setMonth(int month){
		this.month = month;
	}
	public int getYear(){
		return year;
	}
	public void setYear(int year){
		this.year = year;
	}
	public String getDescription(){
		return description;
	}
	public void setDescription(String description){
		this.description = description;
	}
	public int getPayment_Type_Id(){
		return payment_type_id;
	}
	public void setPayment_type_Id(int payment_type_id){
		this.payment_type_id = payment_type_id;
	}
	public double getValue(){
		return value;
	}
	public void setValue(double value){
		this.value = value;
	}	
}