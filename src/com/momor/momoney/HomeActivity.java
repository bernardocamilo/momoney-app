package com.momor.momoney;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.savagelook.android.UrlJsonAsyncTask;

public class HomeActivity extends SherlockActivity {

	private final static String CATEGORIES_URL = "http://10.0.2.2:3000/api/data.json";
	private static final String LOGOUT_URL = "http://10.0.2.2:3000/api/sessions.json";
	private SharedPreferences mPreferences;
	private int id_category;
	ArrayList<Expense> expensesArray;
	ArrayList<PaymentType> paymentTypesArray;
	ArrayList<Category> categoriesArray;
	ArrayList<Integer> categoriesIdExp;
	private AlertDialog alert1;
	private AlertDialog alert2;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
	    getSupportActionBar().setHomeButtonEnabled(true);

		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
	}

	@Override
	public void onResume() {
		super.onResume();

		if (mPreferences.contains("AuthToken")) {
			loadCategoriesFromAPI(CATEGORIES_URL);
		} else {
			Intent intent = new Intent(HomeActivity.this, WelcomeActivity.class);
			startActivityForResult(intent, 0);
		}
	}

    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        getSupportMenuInflater().inflate(R.menu.home, menu);
        return super.onCreateOptionsMenu(menu);
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent_credits = new Intent(HomeActivity.this, CreditsActivity.class);
			startActivityForResult(intent_credits, 0);
			return true;
		case R.id.menu_new_expense:
			Bundle bundle = new Bundle();
    		bundle.putParcelableArrayList("Categories", categoriesArray);
    		bundle.putParcelableArrayList("PaymentTypes", paymentTypesArray);
    		Intent intent_expense = new Intent(HomeActivity.this, NewExpenseActivity.class);
			intent_expense.putExtra("bundleData", bundle);
			startActivityForResult(intent_expense, 0);
			return true;
		case R.id.menu_new_reminder:
			Intent intent_new_reminder = new Intent(HomeActivity.this, NewReminderActivity.class);
			startActivityForResult(intent_new_reminder, 0);
			return true;
		case R.id.menu_new_payment_type:
			Intent intent_payment = new Intent(HomeActivity.this, NewPaymentTypeActivity.class);
			startActivityForResult(intent_payment, 0);
			return true;
		case R.id.menu_new_category:
			Intent intent_category = new Intent(HomeActivity.this, NewCategoryActivity.class);
			startActivityForResult(intent_category, 0);
			return true;
		case R.id.menu_reminders:
			Intent intent_reminders = new Intent(HomeActivity.this, RemindersActivity.class);
			startActivityForResult(intent_reminders, 0);
			return true;
		case R.id.menu_refresh:
			loadCategoriesFromAPI(CATEGORIES_URL);
			return true;
		case R.id.menu_logout:
			logoutFromAPI(LOGOUT_URL);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void alert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.title_alert);
		builder.setMessage(R.string.message_alert);
		builder.setPositiveButton(R.string.button1_alert, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				Bundle bundle = new Bundle();
	    		bundle.putParcelableArrayList("Categories", categoriesArray);
	    		bundle.putParcelableArrayList("PaymentTypes", paymentTypesArray);
	    		Intent intent_expense = new Intent(HomeActivity.this, NewExpenseActivity.class);
				intent_expense.putExtra("bundleData", bundle);
				startActivityForResult(intent_expense, 0);
			}
		});
		builder.setNegativeButton(R.string.button2_alert, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				alert1.dismiss();
			}
		});
		alert1 = builder.create();
		alert1.show();
	}
	
	private void initialAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.title_initial);
		builder.setMessage(R.string.message_initial);
		builder.setPositiveButton(R.string.button1_initial, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				Intent intent_category = new Intent(HomeActivity.this, NewCategoryActivity.class);
				startActivityForResult(intent_category, 0);
			}
		});
		builder.setNegativeButton(R.string.button2_initial, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				Intent intent_payment = new Intent(HomeActivity.this, NewPaymentTypeActivity.class);
				startActivityForResult(intent_payment, 0);
			}
		});
		alert2 = builder.create();
		alert2.show();
	}

	private void logoutFromAPI(String url) {
		LogoutTask logoutTask = new LogoutTask(HomeActivity.this);
		logoutTask.setMessageLoading(getResources().getString(R.string.logging_out));
		logoutTask.execute(url);

	}

	private void loadCategoriesFromAPI(String url) {		
	    GetCategoriesTask getCategoriesTask = new GetCategoriesTask(HomeActivity.this);
	    getCategoriesTask.execute(url + "?auth_token=" + mPreferences.getString("AuthToken", ""));
	}

	private class GetCategoriesTask extends UrlJsonAsyncTask {
	    public GetCategoriesTask(Context context) {
	        super(context);
	    }
	    @Override
	        protected void onPostExecute(JSONObject json) {
	    		try {
	    			JSONArray jsonExpenses = json.getJSONObject("data").getJSONArray("expenses");
	    			JSONObject jsonExpense = new JSONObject();
	    			int length_expense = jsonExpenses.length();
	    			categoriesIdExp = new ArrayList<Integer>(length_expense);
	    			expensesArray = new ArrayList<Expense>(length_expense);
	    			for (int i = 0; i < length_expense; i++) {
	    				jsonExpense = jsonExpenses.getJSONObject(i);
	    				categoriesIdExp.add(jsonExpense.getInt("category_id"));
	    				expensesArray.add(new Expense(jsonExpense.getInt("id"), jsonExpense.getInt("category_id"), 
	    						jsonExpense.getInt("day"), jsonExpense.getInt("month"), jsonExpense.getInt("year"), 
	    						jsonExpense.getString("description"), jsonExpense.getInt("payment_type_id"), 
	    						jsonExpense.getDouble("value")));
	    			}
	    		} catch (Exception e) {
	    			alert();
	    		}
	            try {
	                JSONArray jsonCategories = json.getJSONObject("data").getJSONArray("categories");
	                JSONObject jsonCategory = new JSONObject();
	                int length = jsonCategories.length();
	                ArrayList<String> expensesCategories = new ArrayList<String>(length);
	            	categoriesArray = new ArrayList<Category>(length);
	                for (int i = 0; i < length; i++) {
	                	jsonCategory = jsonCategories.getJSONObject(i);
	                	String categoryName = jsonCategory.getString("category_name");
	                	categoriesArray.add(new Category(jsonCategory.getInt("id"), jsonCategory.getString("category_name")));
	                	if (!expensesCategories.contains(categoryName)) {
	                		expensesCategories.add(categoryName);
	                	}
                	final ListView categoriesListView = (ListView) findViewById (R.id.categories_list_view);
	                if (categoriesListView != null) {
	                    categoriesListView.setAdapter(new ArrayAdapter<String>(HomeActivity.this,
	                      android.R.layout.simple_list_item_1, expensesCategories));
	                    categoriesListView.setOnItemClickListener(new OnItemClickListener(){
	                    	@Override
	            			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	                    		String text = categoriesListView.getItemAtPosition(position).toString();
	                    		int size = categoriesArray.size();
	                    		Bundle bundle = new Bundle();
	                    		bundle.putParcelableArrayList("Expenses", expensesArray);
	                    		for (int j = 0; j< size; j++){
	                    			String categoryName = (categoriesArray.get(j)).getName();
	                    			if (categoryName.equals(text)){
	                    				id_category = (categoriesArray.get(j)).getId();
	                    				bundle.putString("Category_Name", categoryName);
	                    			}	
	                    			bundle.putInt("Category_Id", id_category);
	                    		};
	                    		if (categoriesIdExp.contains(id_category)){
	                    			Intent intent_categories = new Intent(HomeActivity.this, ChartActivity.class);
	                    			intent_categories.putExtra("bundleCategExp", bundle);
		                    		startActivityForResult(intent_categories, 0);
	                    		}
	                    		else{
	                    			alert();
	                    		}
	                    	}
	                    });
	                }
	                }
	                JSONArray jsonPaymentTypes = json.getJSONObject("data").getJSONArray("payment_types");
	                JSONObject jsonPaymentType = new JSONObject();
	                int length_payment = jsonPaymentTypes.length();
	                paymentTypesArray = new ArrayList<PaymentType>(length_payment);
	                for (int i = 0; i < length_payment; i++) {
	                	jsonPaymentType = jsonPaymentTypes.getJSONObject(i);
	                	paymentTypesArray.add(new PaymentType(jsonPaymentType.getInt("id"), jsonPaymentType.getString("payment_type")));
	                }
	            } catch (Exception e) {
	            	alert1.dismiss();
	            	initialAlert();
	        } finally {
	            super.onPostExecute(json);
	        }
	    }
	}
	
	private class LogoutTask extends UrlJsonAsyncTask {
		public LogoutTask(Context context) {
			super(context);
		}
		@Override
		protected JSONObject doInBackground(String... urls) {
			DefaultHttpClient client = new DefaultHttpClient();
			HttpDelete delete = new HttpDelete(urls[0]);
			String response = null;
			JSONObject json = new JSONObject();
			try {
				try {
					json.put("success", false);
					json.put("info", "Something went wrong. Retry!");
					delete.setHeader("Accept", "application/json");
					delete.setHeader("Content-Type", "application/json");
					delete.setHeader("Authorization", "Token token="
							+ mPreferences.getString("AuthToken", ""));

					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					response = client.execute(delete, responseHandler);
					json = new JSONObject(response);

				} catch (HttpResponseException e) {
					e.printStackTrace();
					Log.e("ClientProtocol", "" + e);
				} catch (IOException e) {
					e.printStackTrace();
					Log.e("IO", "" + e);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("JSON", "" + e);
			}
			return json;
		}
		@Override
		protected void onPostExecute(JSONObject json) {
			try {
				if (json.getBoolean("success")) {
					SharedPreferences.Editor editor = mPreferences.edit();
					editor.remove("AuthToken");
					editor.commit();
					Intent intent = new Intent(HomeActivity.this,
							WelcomeActivity.class);
					startActivityForResult(intent, 0);
				}
				Toast.makeText(context, json.getString("info"),
						Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT)
						.show();
			} finally {
				super.onPostExecute(json);
			}
		}
	}
}
