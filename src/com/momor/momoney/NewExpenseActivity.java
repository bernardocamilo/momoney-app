package com.momor.momoney;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.savagelook.android.UrlJsonAsyncTask;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class NewExpenseActivity extends Activity {
	
	 private final static String CREATE_EXPENSE_ENDPOINT_URL = "http://10.0.2.2:3000/api/expenses.json";
	 private SharedPreferences mPreferences;
	 private Integer mExpenseCategoryId;
	 private Integer mExpenseDay;
	 private Integer mExpenseMonth;
	 private Integer mExpensePaymentTypeId;
	 private Float mExpenseValue;
	 private Integer mExpenseYear;
	 private String mExpenseDescription;
	 ArrayList<PaymentType> paymentTypesArray;
	 ArrayList<Category> categoriesArray;
	 private DatePicker picker;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_expense);
		picker = (DatePicker) findViewById(R.id.datePicker);
		Intent intent_data = getIntent();
		Bundle bundle = intent_data.getBundleExtra("bundleData");
		categoriesArray = bundle.getParcelableArrayList("Categories");
		paymentTypesArray = bundle.getParcelableArrayList("PaymentTypes");
		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
		addItemsOnSpinnerCategory();
		addItemsOnSpinnerPaymentType();
	}
	
	public void addItemsOnSpinnerCategory() {
		Spinner spinner = (Spinner) findViewById(R.id.expenseCategorySpinner);
		int size = categoriesArray.size();
		final ArrayList<String> categories = new ArrayList<String>(size);
		final ArrayList<Integer> categoriesId = new ArrayList<Integer>(size);
		for (int i = 0; i < size; i++){
			categories.add(categoriesArray.get(i).getName());
			categoriesId.add(categoriesArray.get(i).getId());
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				mExpenseCategoryId = categoriesId.get(pos);
			}
			@Override
			  public void onNothingSelected(AdapterView<?> arg0) {
			  }
		});
	}
	
	public void addItemsOnSpinnerPaymentType() {
		Spinner spinner = (Spinner) findViewById(R.id.expensePaymentTypeSpinner);
		int size = paymentTypesArray.size();
		final ArrayList<String> paymentTypes = new ArrayList<String>(size);
		final ArrayList<Integer> paymentTypesId = new ArrayList<Integer>(size);
		for (int i = 0; i < size; i++){
			paymentTypes.add(paymentTypesArray.get(i).getPaymentType());
			paymentTypesId.add(paymentTypesArray.get(i).getId());
		}
		ArrayAdapter<String> data2Adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, paymentTypes);
		data2Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(data2Adapter);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				mExpensePaymentTypeId = paymentTypesId.get(pos);
			}
			@Override
			  public void onNothingSelected(AdapterView<?> arg0) {
			  }
		});
	}
	
	public void saveExpense(View button) {
		mExpenseDay = picker.getDayOfMonth();
		mExpenseMonth = picker.getMonth();
		mExpenseYear = picker.getYear();
	    EditText expenseValueField = (EditText) findViewById(R.id.expenseValue);
	    if (expenseValueField.length() == 0) {
	      Toast.makeText(this, getResources().getString(R.string.fill_the_value),
	          Toast.LENGTH_LONG).show();
	      return;
	    } else {
	      mExpenseValue = Float.parseFloat(expenseValueField.getText().toString());
	      EditText expenseDescriptionField = (EditText) findViewById(R.id.expenseDescription);
	      mExpenseDescription = expenseDescriptionField.getText().toString();
	      CreateExpenseTask createExpense = new CreateExpenseTask(NewExpenseActivity.this);
	      createExpense.setMessageLoading(getResources().getString(R.string.creating_new_expense));
	      createExpense.execute(CREATE_EXPENSE_ENDPOINT_URL);
	    }
	  }

	  private class CreateExpenseTask extends UrlJsonAsyncTask {
	    public CreateExpenseTask(Context context) {
	      super(context);
	    }
	    @Override
	    protected JSONObject doInBackground(String... urls) {
	      DefaultHttpClient client = new DefaultHttpClient();
	      HttpPost post = new HttpPost(urls[0]);
	      JSONObject holder = new JSONObject();
	      JSONObject ExpenseObj = new JSONObject();
	      String response = null;
	      JSONObject json = new JSONObject();
	      try {
	        try {
	          json.put("success", false);
	          json.put("info", "Something went wrong. Retry!");
	          ExpenseObj.put("category_id", mExpenseCategoryId);
	          ExpenseObj.put("day", mExpenseDay);
	          ExpenseObj.put("month", mExpenseMonth);
	          ExpenseObj.put("payment_type_id", mExpensePaymentTypeId);
	          ExpenseObj.put("value", mExpenseValue);
	          ExpenseObj.put("year", mExpenseYear);
	          ExpenseObj.put("description", mExpenseDescription);
	          holder.put("expense", ExpenseObj);
	          StringEntity se = new StringEntity(holder.toString());
	          post.setEntity(se);
	          post.setHeader("Accept", "application/json");
	          post.setHeader("Content-Type", "application/json");
	          post.setHeader("Authorization", "Token token=" + mPreferences.getString("AuthToken", ""));
	          ResponseHandler<String> responseHandler = new BasicResponseHandler();
	          response = client.execute(post, responseHandler);
	          json = new JSONObject(response);

	        } catch (HttpResponseException e) {
	          e.printStackTrace();
	          Log.e("ClientProtocol", "" + e);
	        } catch (IOException e) {
	          e.printStackTrace();
	          Log.e("IO", "" + e);
	        }
	      } catch (JSONException e) {
	        e.printStackTrace();
	          Log.e("JSON", "" + e);
	      }
	      return json;
	    }
	    @Override
	      protected void onPostExecute(JSONObject json) {
	        try {
	          if (json.getBoolean("success")) {
	          Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
	          startActivity(intent);
	          finish();
	          }
	          Toast.makeText(context, json.getString("info"), Toast.LENGTH_SHORT).show();
	        } catch (Exception e) {
	        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
	      } finally {
	        super.onPostExecute(json);
	      }
	    }
	  }
}
