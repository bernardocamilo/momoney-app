package com.momor.momoney;

import java.io.IOException;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.savagelook.android.UrlJsonAsyncTask;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class NewReminderActivity extends Activity {

	private final static String CREATE_REMINDER_ENDPOINT_URL = "http://10.0.2.2:3000/api/reminders.json";
	private SharedPreferences mPreferences;
	private String mReminderDescription;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_reminder);
		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
	}

	public void saveReminder(View button) {
		EditText reminderDescriptionlField = (EditText) findViewById(R.id.reminderDescription);
		mReminderDescription = reminderDescriptionlField.getText().toString();
		if (mReminderDescription.length() == 0) {
			Toast.makeText(this,
					getResources().getString(R.string.fill_the_reminder_description),
					Toast.LENGTH_LONG).show();
			return;
		} else {
			CreateReminderTask createReminder = new CreateReminderTask(NewReminderActivity.this);
			createReminder.setMessageLoading(getResources().getString(R.string.creating_new_reminder));
			createReminder.execute(CREATE_REMINDER_ENDPOINT_URL);
		}
	}

	private class CreateReminderTask extends UrlJsonAsyncTask {
		public CreateReminderTask(Context context) {
			super(context);
		}
		@Override
		protected JSONObject doInBackground(String... urls) {
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(urls[0]);
			JSONObject holder = new JSONObject();
			JSONObject reminderObj = new JSONObject();
			String response = null;
			JSONObject json = new JSONObject();
			try {
				try {
					json.put("success", false);
					json.put("info", "Something went wrong. Retry!");
					reminderObj.put("description", mReminderDescription);
					holder.put("reminder", reminderObj);
					StringEntity se = new StringEntity(holder.toString());
					post.setEntity(se);
					post.setHeader("Accept", "application/json");
					post.setHeader("Content-Type", "application/json");
					post.setHeader("Authorization", "Token token="
							+ mPreferences.getString("AuthToken", ""));
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					response = client.execute(post, responseHandler);
					json = new JSONObject(response);
				} catch (HttpResponseException e) {
					e.printStackTrace();
					Log.e("ClientProtocol", "" + e);
				} catch (IOException e) {
					e.printStackTrace();
					Log.e("IO", "" + e);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("JSON", "" + e);
			}
			return json;
		}
		@Override
		protected void onPostExecute(JSONObject json) {
			try {
				if (json.getBoolean("success")) {
					Intent intent = new Intent(getApplicationContext(),
							HomeActivity.class);
					startActivity(intent);
					finish();
				}
				Toast.makeText(context, json.getString("info"),
						Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG)
						.show();
			} finally {
				super.onPostExecute(json);
			}
		}
	}
}
