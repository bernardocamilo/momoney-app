package com.momor.momoney;

import android.os.Parcel;
import android.os.Parcelable;

public class PaymentType implements Parcelable{
	private int id;
	private String payment_type;
	
	public int describeContents(){
		return 0;
	}
	
	public void writeToParcel(Parcel out, int flags){
		out.writeInt(id);
		out.writeString(payment_type);
	}
	
	public static final Parcelable.Creator<PaymentType> CREATOR = new Parcelable.Creator<PaymentType>() {
		public PaymentType createFromParcel(Parcel in) {
			return new PaymentType(in);
		}
		public PaymentType[] newArray(int size){
			return new PaymentType[size];
		}
	};
	
	public PaymentType(Parcel in){
		id = in.readInt();
		payment_type = in.readString();
	}
	
	public PaymentType(int id, String payment_type) {
		this.id = id;
		this.payment_type = payment_type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPaymentType() {
		return payment_type;
	}

	public void setPaymentType(String payment_type) {
		this.payment_type = payment_type;
	}
}
