package com.momor.momoney;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import com.savagelook.android.UrlJsonAsyncTask;

public class RemindersActivity extends Activity {

	private static final String REMINDERS_URL = "http://10.0.2.2:3000/api/reminders.json";
	private static final String TOGGLE_REMINDERS_URL = "http://10.0.2.2:3000/api/reminders/";
	private SharedPreferences mPreferences;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reminder);
		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
	}

	@Override
	public void onResume() {
		super.onResume();

		if (mPreferences.contains("AuthToken")) {
			loadRemindersFromAPI(REMINDERS_URL);
		} else {
			Intent intent = new Intent(RemindersActivity.this, WelcomeActivity.class);
			startActivityForResult(intent, 0);
		}
	}

	private void loadRemindersFromAPI(String url) {
		GetRemindersTask getRemindersTask = new GetRemindersTask(RemindersActivity.this);
		getRemindersTask.setMessageLoading(getResources().getString(R.string.loading_reminders));
		getRemindersTask.execute(url + "?auth_token=" + mPreferences.getString("AuthToken", ""));
	}

	private void toggleRemindersWithAPI(String url) {
		ToggleReminderTask completeRemindersTask = new ToggleReminderTask(RemindersActivity.this);
		completeRemindersTask.setMessageLoading(getResources().getString(R.string.updating_reminders));
		completeRemindersTask.execute(url);
	}

	private class ReminderAdapter extends ArrayAdapter<Reminder> implements
			OnClickListener {
		private ArrayList<Reminder> items;
		private int layoutResourceId;
		public ReminderAdapter(Context context, int layoutResourceId,
				ArrayList<Reminder> items) {
			super(context, layoutResourceId, items);
			this.layoutResourceId = layoutResourceId;
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = (CheckedTextView) layoutInflater.inflate(
						layoutResourceId, null);
			}
			Reminder reminder = items.get(position);
			if (reminder != null) {
				CheckedTextView reminderCheckedTextView = (CheckedTextView) view
						.findViewById(android.R.id.text1);
				if (reminderCheckedTextView != null) {
					reminderCheckedTextView.setText(reminder.getDescription());
					reminderCheckedTextView.setChecked(reminder.getCompleted());
					reminderCheckedTextView.setOnClickListener(this);
				}
				view.setTag(reminder.getId());
			}
			return view;
		}

		@Override
		public void onClick(View view) {
			CheckedTextView reminderCheckedTextView = (CheckedTextView) view
					.findViewById(android.R.id.text1);
			if (reminderCheckedTextView.isChecked()) {
				reminderCheckedTextView.setChecked(false);
				toggleRemindersWithAPI(TOGGLE_REMINDERS_URL + view.getTag()
						+ "/open.json");
			} else {
				reminderCheckedTextView.setChecked(true);
				toggleRemindersWithAPI(TOGGLE_REMINDERS_URL + view.getTag()
						+ "/complete.json");
			}

		}
	}

	private class GetRemindersTask extends UrlJsonAsyncTask {
		public GetRemindersTask(Context context) {
			super(context);
		}
		@Override
		protected void onPostExecute(JSONObject json) {
			try {
				JSONArray jsonReminders = json.getJSONObject("data").getJSONArray(
						"reminders");
				JSONObject jsonReminder = new JSONObject();
				int length = jsonReminders.length();
				final ArrayList<Reminder> remindersArray = new ArrayList<Reminder>(length);

				for (int i = 0; i < length; i++) {
					jsonReminder = jsonReminders.getJSONObject(i);
					remindersArray.add(new Reminder(jsonReminder.getLong("id"), jsonReminder
							.getString("description"), jsonReminder
							.getBoolean("completed")));
				}
				ListView remindersListView = (ListView) findViewById(R.id.reminders_list_view);
				if (remindersListView != null) {
					remindersListView.setAdapter(new ReminderAdapter(RemindersActivity.this,
							android.R.layout.simple_list_item_checked,
							remindersArray));
				}
			} catch (Exception e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT)
						.show();
			} finally {
				super.onPostExecute(json);
			}
		}
	}

	private class ToggleReminderTask extends UrlJsonAsyncTask {
		public ToggleReminderTask(Context context) {
			super(context);
		}
		@Override
		protected JSONObject doInBackground(String... urls) {
			DefaultHttpClient client = new DefaultHttpClient();
			HttpPut put = new HttpPut(urls[0]);
			String response = null;
			JSONObject json = new JSONObject();
			try {
				try {
					json.put("success", false);
					json.put("info", "Something went wrong. Retry!");
					put.setHeader("Accept", "application/json");
					put.setHeader("Content-Type", "application/json");
					put.setHeader("Authorization", "Token token="
							+ mPreferences.getString("AuthToken", ""));

					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					response = client.execute(put, responseHandler);
					json = new JSONObject(response);

				} catch (HttpResponseException e) {
					e.printStackTrace();
					Log.e("ClientProtocol", "" + e);
				} catch (IOException e) {
					e.printStackTrace();
					Log.e("IO", "" + e);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("JSON", "" + e);
			}
			return json;
		}
		@Override
		protected void onPostExecute(JSONObject json) {
			try {
				Toast.makeText(context, json.getString("info"),
						Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT)
						.show();
			} finally {
				super.onPostExecute(json);
			}
		}
	}
}
